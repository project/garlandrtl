<?php
/**
 * Sets the body-tag class attribute.
 *
 * Adds 'sidebar-left', 'sidebar-right' or 'sidebars' classes as needed.
 */
function phptemplate_body_class($sidebar_left, $sidebar_right) {
  if ($sidebar_left != '' && $sidebar_right != '') {
    $class = 'sidebars';
  }
  else {
    if ($sidebar_left != '') {
      $class = 'sidebar-left';
    }
    if ($sidebar_right != '') {
      $class = 'sidebar-right';
    }
  }

  if (isset($class)) {
    print ' class="'. $class .'"';
  }
}

/**
 * Return a themed breadcrumb trail.
 *
 * @param $breadcrumb
 *   An array containing the breadcrumb links.
 * @return a string containing the breadcrumb output.
 */
function phptemplate_breadcrumb($breadcrumb) {
  if (!empty($breadcrumb)) {
    return '<div class="breadcrumb">'. implode(' › ', $breadcrumb) .'</div>';
  }
}

/**
 * Allow themable wrapping of all comments.
 */
function phptemplate_comment_wrapper($content, $type = null) {
  static $node_type;
  if (isset($type)) $node_type = $type;

  if (!$content || $node_type == 'forum') {
    return '<div id="comments">'. $content . '</div>';
  }
  else {
    return '<div id="comments"><h2 class="comments">'. t('Comments') .'</h2>'. $content .'</div>';
  }
}

/**
 * Override or insert PHPTemplate variables into the templates.
 */
function _phptemplate_variables($hook, $vars) {
  if ($hook == 'page') {

    if ($secondary = menu_secondary_local_tasks()) {
      $output = '<span class="clear"></span>';
      $output .= "<ul class=\"tabs secondary\">\n". $secondary ."</ul>\n";
      $vars['tabs2'] = $output;
    }

    // Hook into color.module
    if (module_exists('color')) {
      _color_page_alter($vars);
    }
    return $vars;
  }
  return array();
}

/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs.
 *
 * @ingroup themeable
 */
function phptemplate_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= "<ul class=\"tabs primary\">\n". $primary ."</ul>\n";
  }

  return $output;
}

function garlandrtl_is_rtl_lang ($language) {
  $rtl_langs = array("ar", "fa", "he", "ur", "yi"); // There are a couple or so more. Put the codes of your RTL languages here
  if (module_exists ('locale') ) {
    if (isset ($language)) {
      return in_array ($language, $rtl_langs); 
    }
  }
  return FALSE;
}

// Add the neccessary styles (RTL or LTR) according to the language direction
function garlandrtl_add_styles ($language, $css) {
  if (garlandrtl_is_rtl_lang ($language)) { // If the language of the current page is RTL.
    // Then load the RTL styles instead of default ones.
    
    $directory = path_to_theme();    
    // Delete the original stylesheet if a  customized color stylesheet exists (it's duplicate).
    if( $css['all']['theme'][ variable_get('color_garlandrtl_stylesheet', NULL)] == 1) {
     unset ($css['all']['theme'][$directory . '/style.css']);
    }

    //  if ($is_customized_color) unset ($css)
    foreach($css as $media => $types){
      foreach($types as $type => $files){
        foreach($files as $file => $preprocess){
          $element = $file;
          // Try to locate the RTL  style in the theme's directory. If not found, try to find it in the same directory as the original file. 
          $rtl_style = $directory . '/css/' . str_replace(".css","-rtl.css",basename($file));
          if (!file_exists ($rtl_style)) {
            $rtl_style = dirname ($file) . '/' . str_replace('.css','-rtl.css',basename($file));
          }
          if(file_exists($rtl_style)){
            unset($css[$media][$type][$file]);
            // tell drupal_get_css to preprocess it
            $css[$media][$type][$rtl_style] = $preprocess;
          }
        }
      }
    }
    print drupal_get_css($css);
    return TRUE;
  }
  else {
    return FALSE;
  }
}
